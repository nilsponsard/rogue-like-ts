module world {



    class WorldAssets {
        floor: assets.AssetLoader
        wall: assets.AssetLoader
        constructor() {
            this.floor = assets.load("assets/map/floor.png")
            this.wall = assets.load("assets/map/wall.png")
        }
    }
    export class World {
        assets: WorldAssets
        constructor() {
            this.assets = new WorldAssets()

        }
        draw(ctx: CanvasRenderingContext2D) {

        }
    }
}