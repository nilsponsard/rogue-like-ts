module assets {

    const assetsDiv = document.getElementById("assets") as HTMLCanvasElement

    export let assetsMap: Map<string, AssetLoader> = new Map()

    export class AssetLoader {
        private _image: HTMLImageElement
        constructor(path: string) {
            this._image = document.createElement("img")
            this._image.src = path
            this._image.addEventListener("error", (error) => {
                throw "error when loading the asset : " + error
            })
            assetsDiv.appendChild(this._image)
            assetsMap.set(path, this)
        }
        get image(): HTMLImageElement {
            if (this._image.complete) {
                return this._image
            } else {
                console.log("waiting for the asset to load ...")
                while (!this._image.complete) {
                }
                return this._image
            }
        }
    }


    export function load(path: string) {
        return new AssetLoader(path);
    }

}
