module game {
    export class Game {
        world: world.World
        constructor() {
            this.world = new world.World()

        }
        draw(ctx: CanvasRenderingContext2D) {
            this.world.draw(ctx)
        }
    }
}