module main {

    export const canvas = document.getElementById("canvas") as HTMLCanvasElement
    export const ctx = canvas.getContext("2d") as CanvasRenderingContext2D
    export let mainGame: game.Game
    export function draw() {
        mainGame.draw(ctx)
        requestAnimationFrame(draw)

    }
    export function resize() {

        canvas.width = constants.viewport_width
        canvas.height = constants.viewport_height
        let ratio_height = canvas.width / window.innerWidth
        let ratio_width = canvas.height / window.innerHeight
        canvas.style.height = "auto"
        canvas.style.width = "auto"
        console.log(ratio_width, ratio_height)
        if (ratio_height > ratio_width) {
            canvas.style.width = `${window.innerWidth}px`
        }
        else {
            canvas.style.height = `${window.innerHeight}px`
        }
    }
    export function load() {
        resize()
        window.addEventListener("resize", resize)


        mainGame = new game.Game()
        requestAnimationFrame(draw)
    }
}

let readyStateCheckInterval = setInterval(function () {
    if (document.readyState === "complete") {
        clearInterval(readyStateCheckInterval);
        main.load();
    }
}, 10);



